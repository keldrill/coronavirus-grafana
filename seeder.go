// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"encoding/csv"
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/influxdata/influxdb-client-go"
)

var influxdbHost = flag.String("influxdb-host", "http://localhost:8086", "influxdb host")
var influxdbDatabase = flag.String("influxdb-database", "coronavirus", "influxdb database")
var influxdbBucket = *influxdbDatabase
var influxdbOrg = ""
var influxdbToken = ""
var mode = *flag.String("mode", "jhu", "")

func main() {
	flag.Parse()

	influxdb := influxdb2.NewClient(*influxdbHost, influxdbToken)
	defer influxdb.Close()

	if mode == "jhu" {
		syncJHU(influxdb)
	} else {
		log.Fatal("Invalid mode")
	}

	log.Printf("Sync complete")
}

func syncJHU(influxdb influxdb2.InfluxDBClient) {
	dataTypes := []string{"confirmed", "deaths", "recovered"}

	type DataPoint struct {
		Count         int64
		ProvinceState string
		CountryRegion string
		Lat           float64
		Long          float64
	}

	active := make(map[time.Time]map[string]DataPoint)

	writeApi := influxdb.WriteApi(influxdbOrg, influxdbBucket)

	for _, dataType := range dataTypes {
		url := "https://github.com/CSSEGISandData/COVID-19/raw/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_" + dataType + "_global.csv"
		filename := "/tmp/" + dataType + ".csv"

		err := DownloadFile(url, filename)
		if err != nil {
			panic(err)
		}

		file, err := os.Open(filename)
		if err != nil {
			panic(err)
		}

		var dates []time.Time

		reader := csv.NewReader(file)

		for {
			record, err := reader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				panic(err)
			}

			province_state := string(record[0])
			country_region := string(record[1])

			location := country_region
			if province_state != "" {
				location = province_state + ", " + country_region
			}

			log.Println(location, len(record))

			if country_region == "Country/Region" { // read headers
				dates = make([]time.Time, len(record))

				for i, header := range record {
					if i < 4 {
						continue // skip non-date columns
					}

					date, err := time.Parse("1/2/06", header)
					if err != nil {
						panic(err)
					}

					dates[i] = date
				}

				log.Println(dates)
			} else {
				lat := parseFloat(record[2])
				long := parseFloat(record[3])

				for i, data := range record {
					if i < 4 {
						continue // skip non-date columns
					}

					count := parseInt(data)
					date := dates[i]

					// create point using fluent style
					point := influxdb2.NewPointWithMeasurement("jhu").
						AddTag("province_state", province_state).
						AddTag("country_region", country_region).
						AddTag("location", location).
						AddField("lat", lat).
						AddField("long", long).
						AddField(dataType, count).
						SetTime(date)

					// log.Printf("%s %s for %s on %v\n", data, dataType, country_region, date)
					writeApi.WritePoint(point)

					if _, ok := active[date]; !ok {
						active[date] = make(map[string]DataPoint)
					}

					if _, ok := active[date][location]; !ok {
						active[date][location] = DataPoint{
							Count:         0,
							ProvinceState: province_state,
							CountryRegion: country_region,
							Lat:           lat,
							Long:          long,
						}
					}

					dataPoint := active[date][location]

					if dataType == "confirmed" {
						dataPoint.Count += count
					} else {
						dataPoint.Count -= count
					}

					active[date][location] = dataPoint
				}
			}

			writeApi.Flush()
		}

	}

	for date, dataPoints := range active {
		for location, dataPoint := range dataPoints {
			point := influxdb2.NewPointWithMeasurement("jhu").
				AddTag("province_state", dataPoint.ProvinceState).
				AddTag("country_region", dataPoint.CountryRegion).
				AddTag("location", location).
				AddField("lat", dataPoint.Lat).
				AddField("long", dataPoint.Long).
				AddField("active", dataPoint.Count).
				SetTime(date)

			log.Printf("%v active cases for %s on %v\n", dataPoint.Count, location, date)
			writeApi.WritePoint(point)
		}
	}

	writeApi.Flush()
}

func parseFloat(str string) float64 {
	number, err := strconv.ParseFloat(str, 64)
	if err != nil {
		panic(err)
	}

	return number
}

func parseInt(str string) int64 {
	number, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		panic(err)
	}

	return number
}

// DownloadFile will download a url and store it in local filepath.
// It writes to the destination file as it downloads it, without
// loading the entire file into memory.
func DownloadFile(url string, filepath string) error {
	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}
