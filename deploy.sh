#!/bin/bash

set -euxo pipefail

host=$1

go build -v
strip seeder
rsync -avPhzz seeder ${host}:coronavirus-seeder
ssh $host -- sudo -u influxdb influx -execute \"drop database coronavirus\"
ssh $host -- sudo -u influxdb influx -execute \"create database coronavirus\"
ssh $host -- ./coronavirus-seeder
